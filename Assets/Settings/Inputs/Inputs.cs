// GENERATED AUTOMATICALLY FROM 'Assets/Settings/Inputs/Inputs.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Inputs : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Inputs()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Inputs"",
    ""maps"": [
        {
            ""name"": ""gameplay"",
            ""id"": ""29aafd06-324f-466e-b2e8-ad1353323dbf"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""e035d4f5-84a1-43e4-ac1e-265b5e66a504"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack"",
                    ""type"": ""Button"",
                    ""id"": ""59d2c645-a659-49a5-baf7-be63274493f5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""619ec963-7d87-4958-acb5-9f51d2095cda"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Pick"",
                    ""type"": ""Button"",
                    ""id"": ""d01fd317-f54c-4c42-b74a-a42e3bf3520b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Use"",
                    ""type"": ""Button"",
                    ""id"": ""db2b72fc-227a-4d12-b50d-f74c66debd00"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""dadb7e73-9dd3-498c-9f9f-42fd2105c1f7"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6079e1b4-1bda-4197-844f-ab839b13cd9a"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c8d6f629-b76a-4088-a1e7-6207a9360481"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""01721bef-5d63-499b-ba7a-4d7ebe4db0c2"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""20336f2a-0aff-4511-ac96-76884ec39b1e"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e9ab720a-70d0-431e-87d2-ac43ec22e77e"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""character_selection"",
            ""id"": ""a471d07a-cf64-416b-8a74-b6bbd0bccf09"",
            ""actions"": [
                {
                    ""name"": ""Validate"",
                    ""type"": ""Button"",
                    ""id"": ""a2a4af66-a09a-4c62-94b8-4fcbcb0fe7e7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""78351944-f165-4a3f-aae8-3d40d1652650"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""ba824c22-fa56-453a-9ba7-e6ca98d0d982"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cf685bc7-04aa-482a-9775-9335e0d55e0f"",
                    ""path"": ""<SwitchProControllerHID>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""49a34570-b87f-4377-a580-8b2990db81e6"",
                    ""path"": ""<DualShockGamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""141d90e3-7c82-44c2-ba99-579e5dec5859"",
                    ""path"": ""<DualShockGamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d471036-98ff-4451-863c-dfe98790dfab"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""menu"",
            ""id"": ""4c24978d-ab13-4de2-9570-a9b5502fb6ef"",
            ""actions"": [
                {
                    ""name"": ""Validate"",
                    ""type"": ""Button"",
                    ""id"": ""f619b31c-9acb-4f3c-b1ce-3814ea5563d7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""329251ff-39cb-4bd8-93a6-6d55d74ae8ca"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""362f45e5-6e19-44c9-a607-de75b672cde5"",
                    ""path"": ""<SwitchProControllerHID>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cc51c927-21bc-4e8d-9917-ba841b557b14"",
                    ""path"": ""<DualShockGamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""537ccf5f-9108-4d97-95a0-475d1d842cd6"",
                    ""path"": ""<DualShockGamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""end"",
            ""id"": ""8cbf98b4-364f-4f26-ab42-a2456332dcf3"",
            ""actions"": [
                {
                    ""name"": ""Restart"",
                    ""type"": ""Button"",
                    ""id"": ""081d0fc8-a263-4e9e-87eb-ee776734cf28"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""600d6f7a-4f7a-4c80-a32c-75647159e38f"",
                    ""path"": ""<DualShockGamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""73a4e078-5c8f-4bef-a8d2-34d7ae776ead"",
                    ""path"": ""<DualShockGamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ee171bbe-a1ed-48c1-9309-a6deef603960"",
                    ""path"": ""<SwitchProControllerHID>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ec69fc96-fd02-4851-a71a-ff09e4f23607"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Validate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""287de001-85cc-40f6-a961-a7f225cc4efb"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9c37ddfd-d9b0-452f-b1c4-7d7fcc0c4354"",
                    ""path"": ""<SwitchProControllerHID>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8506221a-fd6b-45b0-97a2-60bd0aa204fd"",
                    ""path"": ""<DualShockGamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""26821624-0c14-43c0-a924-e2c372242aba"",
                    ""path"": ""<DualShockGamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // gameplay
        m_gameplay = asset.FindActionMap("gameplay", throwIfNotFound: true);
        m_gameplay_Move = m_gameplay.FindAction("Move", throwIfNotFound: true);
        m_gameplay_Attack = m_gameplay.FindAction("Attack", throwIfNotFound: true);
        m_gameplay_Dash = m_gameplay.FindAction("Dash", throwIfNotFound: true);
        m_gameplay_Pick = m_gameplay.FindAction("Pick", throwIfNotFound: true);
        m_gameplay_Use = m_gameplay.FindAction("Use", throwIfNotFound: true);
        // character_selection
        m_character_selection = asset.FindActionMap("character_selection", throwIfNotFound: true);
        m_character_selection_Validate = m_character_selection.FindAction("Validate", throwIfNotFound: true);
        m_character_selection_Move = m_character_selection.FindAction("Move", throwIfNotFound: true);
        // menu
        m_menu = asset.FindActionMap("menu", throwIfNotFound: true);
        m_menu_Validate = m_menu.FindAction("Validate", throwIfNotFound: true);
        // end
        m_end = asset.FindActionMap("end", throwIfNotFound: true);
        m_end_Restart = m_end.FindAction("Restart", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // gameplay
    private readonly InputActionMap m_gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_gameplay_Move;
    private readonly InputAction m_gameplay_Attack;
    private readonly InputAction m_gameplay_Dash;
    private readonly InputAction m_gameplay_Pick;
    private readonly InputAction m_gameplay_Use;
    public struct GameplayActions
    {
        private @Inputs m_Wrapper;
        public GameplayActions(@Inputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_gameplay_Move;
        public InputAction @Attack => m_Wrapper.m_gameplay_Attack;
        public InputAction @Dash => m_Wrapper.m_gameplay_Dash;
        public InputAction @Pick => m_Wrapper.m_gameplay_Pick;
        public InputAction @Use => m_Wrapper.m_gameplay_Use;
        public InputActionMap Get() { return m_Wrapper.m_gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @Attack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttack;
                @Attack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttack;
                @Attack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnAttack;
                @Dash.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDash;
                @Pick.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPick;
                @Pick.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPick;
                @Pick.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPick;
                @Use.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnUse;
                @Use.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnUse;
                @Use.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnUse;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Attack.started += instance.OnAttack;
                @Attack.performed += instance.OnAttack;
                @Attack.canceled += instance.OnAttack;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @Pick.started += instance.OnPick;
                @Pick.performed += instance.OnPick;
                @Pick.canceled += instance.OnPick;
                @Use.started += instance.OnUse;
                @Use.performed += instance.OnUse;
                @Use.canceled += instance.OnUse;
            }
        }
    }
    public GameplayActions @gameplay => new GameplayActions(this);

    // character_selection
    private readonly InputActionMap m_character_selection;
    private ICharacter_selectionActions m_Character_selectionActionsCallbackInterface;
    private readonly InputAction m_character_selection_Validate;
    private readonly InputAction m_character_selection_Move;
    public struct Character_selectionActions
    {
        private @Inputs m_Wrapper;
        public Character_selectionActions(@Inputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @Validate => m_Wrapper.m_character_selection_Validate;
        public InputAction @Move => m_Wrapper.m_character_selection_Move;
        public InputActionMap Get() { return m_Wrapper.m_character_selection; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Character_selectionActions set) { return set.Get(); }
        public void SetCallbacks(ICharacter_selectionActions instance)
        {
            if (m_Wrapper.m_Character_selectionActionsCallbackInterface != null)
            {
                @Validate.started -= m_Wrapper.m_Character_selectionActionsCallbackInterface.OnValidate;
                @Validate.performed -= m_Wrapper.m_Character_selectionActionsCallbackInterface.OnValidate;
                @Validate.canceled -= m_Wrapper.m_Character_selectionActionsCallbackInterface.OnValidate;
                @Move.started -= m_Wrapper.m_Character_selectionActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_Character_selectionActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_Character_selectionActionsCallbackInterface.OnMove;
            }
            m_Wrapper.m_Character_selectionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Validate.started += instance.OnValidate;
                @Validate.performed += instance.OnValidate;
                @Validate.canceled += instance.OnValidate;
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
            }
        }
    }
    public Character_selectionActions @character_selection => new Character_selectionActions(this);

    // menu
    private readonly InputActionMap m_menu;
    private IMenuActions m_MenuActionsCallbackInterface;
    private readonly InputAction m_menu_Validate;
    public struct MenuActions
    {
        private @Inputs m_Wrapper;
        public MenuActions(@Inputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @Validate => m_Wrapper.m_menu_Validate;
        public InputActionMap Get() { return m_Wrapper.m_menu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
        public void SetCallbacks(IMenuActions instance)
        {
            if (m_Wrapper.m_MenuActionsCallbackInterface != null)
            {
                @Validate.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnValidate;
                @Validate.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnValidate;
                @Validate.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnValidate;
            }
            m_Wrapper.m_MenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Validate.started += instance.OnValidate;
                @Validate.performed += instance.OnValidate;
                @Validate.canceled += instance.OnValidate;
            }
        }
    }
    public MenuActions @menu => new MenuActions(this);

    // end
    private readonly InputActionMap m_end;
    private IEndActions m_EndActionsCallbackInterface;
    private readonly InputAction m_end_Restart;
    public struct EndActions
    {
        private @Inputs m_Wrapper;
        public EndActions(@Inputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @Restart => m_Wrapper.m_end_Restart;
        public InputActionMap Get() { return m_Wrapper.m_end; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(EndActions set) { return set.Get(); }
        public void SetCallbacks(IEndActions instance)
        {
            if (m_Wrapper.m_EndActionsCallbackInterface != null)
            {
                @Restart.started -= m_Wrapper.m_EndActionsCallbackInterface.OnRestart;
                @Restart.performed -= m_Wrapper.m_EndActionsCallbackInterface.OnRestart;
                @Restart.canceled -= m_Wrapper.m_EndActionsCallbackInterface.OnRestart;
            }
            m_Wrapper.m_EndActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Restart.started += instance.OnRestart;
                @Restart.performed += instance.OnRestart;
                @Restart.canceled += instance.OnRestart;
            }
        }
    }
    public EndActions @end => new EndActions(this);
    public interface IGameplayActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnAttack(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnPick(InputAction.CallbackContext context);
        void OnUse(InputAction.CallbackContext context);
    }
    public interface ICharacter_selectionActions
    {
        void OnValidate(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
    }
    public interface IMenuActions
    {
        void OnValidate(InputAction.CallbackContext context);
    }
    public interface IEndActions
    {
        void OnRestart(InputAction.CallbackContext context);
    }
}
