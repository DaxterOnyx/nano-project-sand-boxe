﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadBucket : MonoBehaviour
{
	public float StickToHeadTime;

	private float _timeLeft;
	private CharacterController target;

	public void SetTarget(CharacterController character)
	{
		_timeLeft = StickToHeadTime;
		target = character;
		target?.OnBucketHit(this);
	}

	private void Update()
	{
		if (_timeLeft <= 0.0f)
		{
			target?.OnBucketEscape();
			Destroy(gameObject);
		}
		_timeLeft -= Time.deltaTime;
	}
}
