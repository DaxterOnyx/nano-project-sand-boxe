﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ItemController))]
public abstract class ItemEffect : MonoBehaviour
{
    public event Action<ItemEffect, int> onUseSuccess; // [int] is the remaning uses of the item. Triggers only on use end
    public event Action<ItemEffect> onUseFail; // Triggers both at the use start and use end if it fails
    public event Action<ItemEffect> onNoUsesLeft; // Triggers on use end

    [Header("Global Information")]
    public float UseCooldown = 0.5f;
    [Range(1, 9)] public int MaxUses = 1;

    protected ItemController _item;

    private float _timeBeforeUse;
    private int _usesLeft;

    public int UsesLeft => _usesLeft;

	protected void Awake()
	{
        _item = GetComponent<ItemController>();
        _usesLeft = MaxUses;
    }

	private void OnEnable()
	{
        _timeBeforeUse = UseCooldown;
    }

	public bool StartUse()
	{
        if (_item.Using || _timeBeforeUse > 0.0f) return false;

        bool success = BeginEffect();
        if (!success) onUseFail?.Invoke(this);
        return true;
	}

    public bool StopUse()
    {
        if (!_item.Using) return false;
        _timeBeforeUse = UseCooldown;

        bool success = EndEffect();
        if (!success)
        {
            onUseFail?.Invoke(this);
            return false;
        }

        onUseSuccess?.Invoke(this, _usesLeft);

        if (--_usesLeft == 0)
        {
            onNoUsesLeft?.Invoke(this);
        }

        return true;
    }

	protected void Update()
	{
        _timeBeforeUse -= Time.deltaTime;
	}

    /**
     * Called when the player presses the "Use" button. Starts the item effect
     * Ex : Start displaying wordspace guides
     * (the player slowdown is automatic, no need to implement)
     * 
     * Returns true if the action can begin, false otherwise
     */
    public virtual bool BeginEffect() => true;

    /**
     * Called when the player releases the "Use" button. Ends the item effect
     * Ex : perform a one-shot effect
     * 
     * Returns true if the action can end/perform properly, false otherwise
     * Ex : An already placed castle blocks the construction
     */
    public virtual bool EndEffect() => true;
}
