﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ItemController))]
public class WatergunEffect : ItemEffect
{
    public event Action<BucketEffect> onWatergunAim; // Triggered when starts firing
    public event Action<BucketEffect> onWatergunStop; // Triggered when stops firing

    [Header("Damages")]
    public float DamagesPerTick;
    public float TickDelay;

    [Space]

    public float MaxRange = 6.0f;
    public float BeamMaxDuration = 1.0f;
    public LayerMask CollisionsMask; // Characters and blocking objects
    public AnimationCurve RangeOverTime;

    public GameObject WaterBeamPrefab; // The prefab of water beam
    public Transform Cannon;

    private WaterBeam _currentWaterBeam;
    private float _timeBeforeReload = 0.0f;
    private float _tickCooldown;

    // TODO : clean on fight end

    /**
     * Called when the player presses the "Use" button. Starts the item effect
     * Ex : Start displaying wordspace guides
     * (the player slowdown is automatic, no need to implement)
     * 
     * Returns true if the action can begin, false otherwise
     */
    public override bool BeginEffect()
    {
        _currentWaterBeam = Instantiate(WaterBeamPrefab).GetComponent<WaterBeam>();
        _currentWaterBeam.transform.SetParent(Cannon);
        _currentWaterBeam.transform.localPosition = Vector3.zero;
        _timeBeforeReload = BeamMaxDuration;
        _tickCooldown = TickDelay;

        _currentWaterBeam.SetStart(Cannon.position);
        _currentWaterBeam.SetEnd(Cannon.position, false);

        return true;
    }

	/**
     * Called when the player releases the "Use" button. Ends the item effect
     * Ex : perform a one-shot effect
     * 
     * Returns true if the action can end/perform properly, false otherwise
     * Ex : An already placed castle blocks the construction
     */
	public override bool EndEffect() 
    {
        _currentWaterBeam?.Collapse();
        _currentWaterBeam = null;
        return true;
    }

    /**
     * Checks collisions with the target and damages it
     */
    private bool DoDamages(float currentRange, out float trueRange)
	{
        RaycastHit hit;
        
        if (Physics.Raycast(Cannon.position, _item.Owner.transform.forward, out hit, currentRange, CollisionsMask))
		{
            _tickCooldown -= Time.deltaTime;
            if (_tickCooldown < 0.0f)
            {
                _tickCooldown = TickDelay;
                CharacterController character;
                if (hit.collider.TryGetComponent<CharacterController>(out character))
                {
                    character.Health.Damage(DamagesPerTick, false);
                }
            }

            trueRange = hit.distance;

            return true;
		}

        trueRange = currentRange;

        return false;
	}

    protected void Update()
    {
        base.Update();

        if (!_currentWaterBeam) return;

        float lifePercent = 1 - _timeBeforeReload / BeamMaxDuration;
        float range = RangeOverTime.Evaluate(lifePercent) * MaxRange;

        float trueRange;
        bool hit = DoDamages(range, out trueRange);
        _currentWaterBeam.SetColliding(hit);

        // TODO : show splash when hit

        _currentWaterBeam.SetStart(Cannon.position);
        _currentWaterBeam.SetEnd(Cannon.position + _item.Owner.transform.forward * trueRange);

        _timeBeforeReload -= Time.deltaTime;

        if (_timeBeforeReload <= 0.0f)
        {
            Destroy(_currentWaterBeam.gameObject);
            _currentWaterBeam = null;
        }
    }
}
