﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ItemController))]
public class ShovelEffect : ItemEffect
{
    public event Action<ShovelEffect> onCastleBuild; // Triggered when the castle is built

    public GameObject BuildIndicatorPrefab;
    public float buildDistance = 0.5f;

    [HideInInspector] public ObstaclesManager Obstacles;

    private bool _isConstructible;
    private BuildIndicator _buildIndicator;

    private Vector2Int _targetTile;

    /**
     * Called when the player presses the "Use" button. Starts the item effect
     * Ex : Start displaying wordspace guides
     * (the player slowdown is automatic, no need to implement)
     * 
     * Returns true if the action can begin, false otherwise
     */
    public override bool BeginEffect()
    {
        _buildIndicator = Instantiate(BuildIndicatorPrefab).GetComponent<BuildIndicator>();
        UpdateIndicator();
        return true;
    }

	private void OnDestroy()
	{
        if (_buildIndicator)
            Destroy(_buildIndicator.gameObject);
    }

	protected new void Update()
	{
        base.Update();

        if (!_item.Using || !_buildIndicator) return;

        UpdateIndicator();
	}

    private void UpdateIndicator()
	{
        Vector2Int targetTile = GetBuildTile();

        if (targetTile != _targetTile)
        {
            _buildIndicator.SetPosition(Obstacles.Grid.GetPosition(targetTile));
            _targetTile = targetTile;
        }

        bool constructible = Obstacles.IsConstructible(targetTile);
        if (constructible ^ _isConstructible)
        {
            _buildIndicator.SetCanBuild(constructible);
            _isConstructible = constructible;
        }
    }

    /**
     * Called when the player releases the "Use" button. Ends the item effect
     * Ex : perform a one-shot effect
     * 
     * Returns true if the action can end/perform properly, false otherwise
     * Ex : An already placed castle blocks the construction
     */
    public override bool EndEffect() {

        if (_buildIndicator) {
            Destroy(_buildIndicator.gameObject);
            _buildIndicator = null;
        }

        SandCastle castle;

        if (Obstacles.SpawnSandCastle(GetBuildTile(), out castle)) {
            castle.CheckCollisionsForStun(_item.Owner.GetComponent<CharacterController>());
            onCastleBuild?.Invoke(this);
            return true;
		}
        return false;
    }

    public Vector2Int GetBuildTile()
	{
        Vector3 buildPosition = _item.Owner.transform.position + _item.Owner.transform.forward * buildDistance;
        Vector2Int targetTile = Obstacles.Grid.GetTile(buildPosition);
        return targetTile;
    }
}
