﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnProfile", menuName = "ScriptableObjects/Items/SpawnProfile")]
public class SpawnProfile : SerializedScriptableObject
{
    public int MaxItems = 3;
    [MinMaxSlider(0f, 16f)] public Vector2 SpawnTimeInterval;

    //TODO : Put limitation on the type HandItem
    [DictionaryDrawerSettings(KeyLabel = "Item", ValueLabel = "Probability")]
    public Dictionary<GameObject, float> ItemPool = new Dictionary<GameObject, float>();

    /**
     * Returns a random spawn delay depending on the profile's SpawnTimeInterval
     */
    public float GetRandomDelay()
	{
        return Random.Range(SpawnTimeInterval.x, SpawnTimeInterval.y);
	}

    /**
     * Returns a random item in the list, taking their weight in account
     */
    public GameObject GetRandomItem()
	{
        if (ItemPool.Count == 0) return null;

        float weight = Random.Range(0.0f, _totalWeight);
        float currentWeight = 0.0f;
        
        foreach (GameObject item in ItemPool.Keys)
		{
            currentWeight += ItemPool[item];
            if (weight <= currentWeight)
                return item;
		}

        return null;
	}

    private float _totalWeight
	{
        get
		{
            float totalWeight = 0.0f;
            foreach (float weight in ItemPool.Values)
                totalWeight += weight;
            return totalWeight;
		}
	}
}
