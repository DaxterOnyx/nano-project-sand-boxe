﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SpawnProfileReceiver : MonoBehaviour, INotificationReceiver
{
	[SerializeField] private ItemsManager ItemsManager;

	public void OnNotify(Playable origin, INotification notification, object context)
	{
		if (!(notification is SpawnProfileMarker profileMarker)) return;
		if (!ItemsManager) return;

		ItemsManager.ChangeSpawnProfile(profileMarker.Profile);
	}
}
