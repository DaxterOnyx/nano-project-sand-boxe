﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class SpawnProfileMarker : Marker, INotification
{
	[SerializeField] private SpawnProfile profile;

	public PropertyName id => new PropertyName();

	public SpawnProfile Profile => profile;
}
