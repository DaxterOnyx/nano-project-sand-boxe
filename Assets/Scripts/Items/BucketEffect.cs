﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ItemController))]
public class BucketEffect : ItemEffect
{
    public event Action<BucketEffect> onBucketDrop; // Triggered when the castle is built

    public LayerMask CharacterMask;
    public float DropDistance = 0.5f;
    public float HitboxRadius = 0.4f;

    public GameObject HeadBucketPrefab; // The prefab of the bucket that snaps on the character's head

    // TODO : clean on fight end

    /**
     * Called when the player presses the "Use" button. Starts the item effect
     * Ex : Start displaying wordspace guides
     * (the player slowdown is automatic, no need to implement)
     * 
     * Returns true if the action can begin, false otherwise
     */
    public override bool BeginEffect()
    {
        return true;
    }

    /**
     * Called when the player releases the "Use" button. Ends the item effect
     * Ex : perform a one-shot effect
     * 
     * Returns true if the action can end/perform properly, false otherwise
     * Ex : An already placed castle blocks the construction
     */
    public override bool EndEffect() {

        Vector3 dropPosition = _item.Owner.transform.position + _item.Owner.transform.forward * DropDistance;

        onBucketDrop?.Invoke(this);

        Collider[] hitColliders = Physics.OverlapSphere(dropPosition, HitboxRadius, CharacterMask);
        foreach (Collider collider in hitColliders)
		{
            if (collider.gameObject == _item.Owner.gameObject) continue; // Can't auto bucket !
            
            CharacterController character;
            if (collider.TryGetComponent<CharacterController>(out character))
			{ // If this is not the owner and a character, then we hit the correct target !
                if (character.HasBucketOnHead)
                    continue; // Can't place two buckets on the same head !

                GameObject bucket = Instantiate(HeadBucketPrefab);
                HeadBucket hb = bucket.GetComponent<HeadBucket>();
                hb.SetTarget(character);
                return true;
            }
		}

        return false;
    }
}
