﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
	public static event Action<ItemController> onItemSpawn;
    public event Action<ItemController> onItemBreak;

	public event Action<ItemController, Hand> onPick;
	public event Action<ItemController, ItemController> onDrop;

	public event Action<ItemController> onGrounded; // Called on drop and on creation
	public event Action<ItemController> onStartUse;
	public event Action<ItemController> onStopUse;

    public Hand Owner { get; private set; }
	public bool Using { get; private set; }

	public ItemHandlesManager Handles;
	public ItemEffect Effect;
	public GameObject GroundCircle;

	private void Awake()
	{
		Effect.enabled = false;
		Effect.onNoUsesLeft += OnNoUsesLeft;

		onGrounded += Handles.GroundHandle;
		onStartUse += Handles.HeldHandle;
		onStopUse += Handles.StoredHandle;
	}

	private void OnDestroy()
	{
		Effect.onNoUsesLeft -= OnNoUsesLeft;

		onGrounded -= Handles.GroundHandle;
		onStartUse -= Handles.HeldHandle;
		onStopUse -= Handles.StoredHandle;
	}

	protected void Start()
	{
		ItemController.onItemSpawn?.Invoke(this);
	}

	public void Pick(Hand owner)
	{
		if (Owner) return; // Can't steal item to another player !
		
		Owner = owner;
		Effect.enabled = true;
		GroundCircle.SetActive(false);

		onPick?.Invoke(this, owner);
		Handles.StoredHandle(this);
	}

	/**
	 * Drops the item at another item position
	 * (the item isn't placed itself, it needs to wait for the item manager
	 * to catch the onDrop event and position it)
	 * 
	 * replacement is the new item in the character's hand
	 */
	public void Drop(ItemController replacement)
	{
		Owner = null;
		Effect.enabled = false;

		GroundCircle.SetActive(true);
		onDrop?.Invoke(this, replacement);
		onGrounded?.Invoke(this);
	}

	public bool StartUse()
	{
		if (!Owner) return false;
		
		if (!Effect.StartUse()) return false;

		Using = true;
		onStartUse?.Invoke(this);
		return true;
	}

	public bool StopUse()
	{
		if (!Owner) return false;
		
		bool success = Effect.StopUse();
		Using = false;
		onStopUse?.Invoke(this);
		return success;
	}

	private void OnNoUsesLeft(ItemEffect effect)
	{
		onItemBreak?.Invoke(this);
		Destroy(gameObject);
	}
}
