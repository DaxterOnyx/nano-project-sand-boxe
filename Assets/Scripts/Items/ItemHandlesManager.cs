﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Handles the anchor point transitions (align to hand, to ground, to the player's belt, etc...)
 */
public class ItemHandlesManager : MonoBehaviour
{
    [SerializeField]private Transform ItemMesh;
    
    [PropertySpace]

    [SerializeField] private Transform Ground;
    [SerializeField] private Transform Held;
    [SerializeField] private Transform Stored;

    [Header("Debug")]
    [SerializeField] private float HandleSize = 0.05f;

    /**
     * Attaches the item to a parent
     * pass null to detach
     */
    public void Attach(Transform parent)
	{
        transform.SetParent(parent, true);
        if (!parent) return;

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
	}

    public void GroundHandle(ItemController item)
	{
        ItemMesh.SetParent(Ground, false);
    }

    public void HeldHandle(ItemController item)
    {
        ItemMesh.SetParent(Held, false);
    }

    public void StoredHandle(ItemController item)
    {
        ItemMesh.SetParent(Stored, false);
    }

	private void OnDrawGizmos()
	{
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, HandleSize);
	}
}
