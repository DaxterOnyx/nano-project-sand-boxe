using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Hit", menuName = "ScriptableObjects/Character/Hit", order = 0)]
public class Hit : ScriptableObject
{
    [Min(0)]
    public int Damage;

    public bool Charging = false;
    [Min(0),Tooltip("Damage on charged attack, when it is not charged."),ShowIf("Charging")]
    public int MinDamage;
    [ShowIf("Charging")]
    public AnimationCurve ChargingDamage= AnimationCurve.Linear(0,0,1,1);
    
    public GameObject VFXPrefab;
}