﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class WaterBeam : MonoBehaviour
{
	public Transform beamStart;
	public LineRenderer beam1;
	public LineRenderer beam2;
	public VisualEffect splash;

	public float CollapseFactor = 0.9f;
	private bool _collapsing;
	private int _lineCount;

	private Vector3 _start;

	private void Awake()
	{
		_collapsing = false;
		_lineCount = beam1.positionCount;
	}

	public void SetStart(Vector3 position)
	{
		_start = position;
		beam1.SetPosition(0, position);
		beam2.SetPosition(0, position);
	}

	public void SetEnd(Vector3 position, bool doLerp = true)
	{
		for (int i = 1; i < _lineCount; i++)
		{
			float lerp = (float)(i + 1) / (float)_lineCount;
			Vector3 currentPos = beam1.GetPosition(i);
			Vector3 targetPos = Vector3.Lerp(_start, position, lerp);

			if (doLerp)
				targetPos = Vector3.Lerp(currentPos, targetPos, (1.0f - lerp * 0.90f) + 0.1f);

			beam1.SetPosition(i, targetPos);
			beam2.SetPosition(i, targetPos);
		}

		splash.gameObject.transform.position = position;
	}

	public void SetColliding(bool colliding)
	{
		splash.gameObject.SetActive(colliding);
	}

	private void Update()
	{
		if (!_collapsing) return;
		transform.localScale *= CollapseFactor;

		if (transform.localScale.z <= 0.05f) 
			Destroy(gameObject);
	}

	public void Collapse()
	{
		Destroy(gameObject);
		_collapsing = true;
	}
}
