﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class ItemVFX : MonoBehaviour
{
    public VisualEffect ItemFX;
    public float RotationSpeed;

    void Start()
    {
        ItemFX.Play();
    }

	private void Update()
	{
        transform.RotateAround(transform.position, Vector3.up, RotationSpeed * Time.deltaTime);
	}
}
