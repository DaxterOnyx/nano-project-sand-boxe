﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemIndicator : MonoBehaviour
{
	private void OnEnable()
	{
        LeanTween.scale(gameObject, Vector3.one, 0.3f)
            .setFrom(0)
            .setEaseSpring()
            .setOnComplete(() =>
            {
                LeanTween.rotateAround(gameObject, Vector3.up, 360, 20.5f).setLoopClamp();
                LeanTween.scaleX(gameObject, 0.9f, 1f).setLoopPingPong().setEaseOutQuad();
                LeanTween.scaleZ(gameObject, 0.9f, 1f).setLoopPingPong().setEaseOutQuad();
            });
	}
}
