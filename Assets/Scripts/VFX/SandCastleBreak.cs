﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class SandCastleBreak : MonoBehaviour
{
    public VisualEffect effect;

    void Start()
    {
        effect.Play();
        Destroy(gameObject, 1.0f);
    }
}
