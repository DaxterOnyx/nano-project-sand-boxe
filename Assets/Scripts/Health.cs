﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    // First float is amount, second float is life percent
    public event Action<Health, float, float> onHealthChanged;
    public event Action<Health> onDeath;
    public event Action<bool> OnTakeDamage;

    public float MaxLife;
    
    private float _life;

    public float Amount => _life;

    public void ResetLife()
	{
        _life = MaxLife;
        onHealthChanged?.Invoke(this, _life, 1.0f);
	}

    public void Damage(float amount, bool stun)
	{
        _life -= amount;
        onHealthChanged?.Invoke(this, _life, _life / MaxLife);


        if (_life <= 0.0f)
		{
            onDeath?.Invoke(this);
		}

        OnTakeDamage?.Invoke(stun);
    }
}
