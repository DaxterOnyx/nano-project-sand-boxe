using UnityEngine;
using System;
using System.Collections.Generic;
using System.Data.Common;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

namespace Character
{
    [RequireComponent(typeof(Animator))]
    public class Attack : MonoBehaviour
    {
        internal event Action<float> onStartCharging;
        internal event Action onStopCharging;
        internal event Action onStartAttack;
        internal event Action onPunch;
        internal event Action onKick;

        [SerializeField] private float MinTimeCharging = 0.5f;
        [SerializeField] private float MaxTimeCharging = 2.5f;
        [SerializeField] private float TimeBetweenCombo = 1f;
        [SerializeField] private float Range = 0.5f;
        [SerializeField] private Vector3 HitPos = new Vector3(0, 1.5f, 0);
        [SerializeField] private float HitVFXDurability = 5;

        [ReadOnly, ShowInInspector] private bool _canAttack = true;
        [ReadOnly, ShowInInspector] private bool _canMakeDamage = true;
        [ReadOnly, ShowInInspector] private int _comboState = 0;
        [ReadOnly, ShowInInspector] private bool _isCharging = false;
        [ReadOnly, ShowInInspector] private bool _isChargingAnimation = false;
        [ReadOnly, ShowInInspector] private bool _isPressing = false;
        [ReadOnly, ShowInInspector] private bool _isAttacking = false;

        [ReadOnly, ShowInInspector] private float _timeCharged = 0;
        [ReadOnly, ShowInInspector] private float _timeLastHit = 0;

        internal void DisableAttack()
        {
            Debug.Log("DisableAttack");
            _canAttack = false;
            _canMakeDamage = false;
            _isPressing = false;

            if (_isAttacking)
            {
                _isAttacking = false;
                EndAnticipation();
                OnEndAttack();
            }

            
            _isCharging = false;
            _isChargingAnimation = false;
            _comboState = 0;
            _timeLastHit = 0;
            _timeCharged = 0;

            if(_isCharging || _isChargingAnimation)
            {
                _isCharging = false;
                _isChargingAnimation = false;
                EndCharging();
            }
        }

        internal void EnableAttack()
        {
            _canAttack = true;
        }

        internal void DisableDamage()
        {
            _canMakeDamage = false;
        }

        internal void EnableDamage()
        {
            _canMakeDamage = true;
        }

        private void Update()
        {
            if (_isCharging)
            {
                _timeCharged += Time.deltaTime;
                if (!_isChargingAnimation && _timeCharged >= MinTimeCharging)
                {
                    StartCharging();
                }

                if (_timeCharged >= MaxTimeCharging)
                {
                    EndCharging();
                }
            }
        }

        #region InputGestion

        internal void OnAttackPressed()
        {
            if (_canAttack)
            {
                _isPressing = true;
                onStartAttack?.Invoke();
            }
        }

        internal void OnAttackRelease()
        {
            if (_canAttack)
            {
                _isPressing = false;
                if (_isCharging)
                {
                    EndCharging();
                }

                if (!(Time.time <= _timeLastHit + TimeBetweenCombo))
                {
                    _comboState = 0;
                }
            }
        }

        #endregion

        #region Animation Event

        private void StartAttack()
        {
            _timeLastHit = Time.time;
            if (_comboState < 2)
                onPunch?.Invoke();
            else
                onKick?.Invoke();
        }

        public void StartAnticipation()
        {
            _isAttacking = true;
        }

        public void EndAnticipation()
        {
            if (_isPressing)
            {
                //START COUNT CHARGING
                _isCharging = true;
                _timeCharged = 0;
            }
            else
            {
                StartAttack();
            }
        }

        private void StartCharging()
        {
            _isChargingAnimation = true;
            onStartCharging?.Invoke(MaxTimeCharging);
        }

        private void EndCharging()
        {
            _isPressing = false;
            _isCharging = false;
            _isChargingAnimation = false;

            onStopCharging?.Invoke();
        }

        private void Hit(Hit hit)
        {
            Debug.Log(_canAttack + " " + _canMakeDamage);
            if(!_canAttack) return;
            var myTransform = transform;
            var targets = Physics.OverlapSphere(myTransform.position + myTransform.forward * Range, Range);

            foreach (var target in targets)
            {
                if (target.gameObject == gameObject)
                    continue;

                Health targetHealth = target.GetComponent<Health>();
                if (!(targetHealth is null))
                {
                    MakeDamage(targetHealth,hit);
                }
            }

            if (!hit.Charging && _comboState < 2)
                _comboState++;
            else
                _comboState = 0;
        }

        private void OnEndAttack()
        {
            _isAttacking = false;
        }

        #endregion

        private void MakeDamage(Health target, Hit hit)
        {
            var vfx = Instantiate(hit.VFXPrefab, target.transform.position + HitPos, Quaternion.identity,
                target.transform);

            int hitDamage = hit.Damage;
            bool stun = _comboState == 2;
            if (hit.Charging)
            {
                stun = _timeCharged >= MaxTimeCharging;
                var timeCharged = (_timeCharged) / MaxTimeCharging;
                var f = hit.ChargingDamage.Evaluate(timeCharged);
                hitDamage = Mathf.RoundToInt(hit.MinDamage +
                                             ((hit.Damage - hit.MinDamage) *
                                              f));
                vfx.GetComponentInChildren<VisualEffect>().SetFloat("Charge", f);
            }

            if (_canMakeDamage)
            {
                target.Damage(hitDamage, stun);
            }
            else
            {
                target.Damage(0, false);
            }

            Destroy(vfx, HitVFXDurability);
        }
    }
}