﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Character;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterController : MonoBehaviour
{
    public event Action<CharacterController> onKnockout;

    public Transform RoundStartPosition;

    public CharacterMovement Movement;
    public Hand Hand;
    public Health Health;

    public Transform BucketPosition;

    public CharacterHUD HUD;

    private InputRelay _boundRelay;

    public Animator Animator;

    public Attack Attack;

    public FXManager FXManager;

    public CharacterSound Sound;
    
    [HideInInspector] public CharacterController Opponent;
    [HideInInspector] public int RoundsWon;
    [HideInInspector] public bool HasBucketOnHead;

    public bool IsBoundToInput => _boundRelay;
    private bool _isInCutscene = false;

    private void Awake()
    {
        RoundState.onRoundStart += OnRoundStart;
        RoundState.onFightStart += OnFightStart;
        RoundState.onFightEnd += OnFightEnd;
        
        //HUD EVENT
        if (HUD is null)
        {
            Debug.LogWarning("Without HUD");
        }
        else
        {
            //HAND EVENT
            Hand.onItemChange += HUD.OnItemChange;
            Hand.onStopUse += HUD.OnItemUse;
        
            //HEALTH EVENT
            Health.onHealthChanged += HUD.OnHealthbarUpdate;
        }
        
        //HAND EVENT
        Hand.onStartUse += Movement.OnStartUsing;
        Hand.onStopUse += Movement.OnStopUsing;
        Hand.onItemChange += Movement.OnItemChange;

        //HEALTH EVENT
        Health.onDeath += Knockout;
        Health.OnTakeDamage += OnTakeDamage;

        //ATTACK EVENT
        Attack.onStartCharging += OnStartCharging;
        Attack.onStopCharging += OnStopCharging;
        Attack.onStartAttack += OnStartAttack;
        Attack.onPunch += OnPunch;
        Attack.onKick += OnKick;

        //MOVEMENT EVENT
        Movement.onStartMove += OnMove;
        Movement.onStopMove += OnMove;
        Movement.onDashStart += OnStartDash;
        Movement.onDashEnd += OnEndDash;
        Movement.onStunStart += OnStartStun;
        Movement.onStunEnd += OnStunStop;
    }

    private void OnStunStop()
    {
        Attack.EnableAttack();
    }

    private void OnStartStun(float duration)
    {
        FXManager.StartStun(duration);
        Attack.DisableAttack();
    }

    private void OnTakeDamage(bool stun)
    {
        Animator.Play("Take Damage");
        Movement.MoveStop();
        if (stun)
        {
            Movement.Stun(1f);

        }
        Attack.DisableAttack();
    }

    private void EndTakeDamage() 
    {
        
        Animator.Play("IdleRun");
        Attack.EnableAttack();
        Attack.EnableDamage();
    }
        

    private void Start()
    {
        if (FXManager == null)
            FXManager = GetComponent<FXManager>();

        Movement.onTurn += FXManager.OnTurn;

        RoundsWon = 0;
    }

    private void OnDestroy()
    {
        RoundState.onRoundStart -= OnRoundStart;
        RoundState.onFightStart -= OnFightStart;
        RoundState.onFightEnd -= OnFightEnd;
    }

    public void BindInputRelay(InputRelay relay)
    {
        _boundRelay = relay;
        relay.Input.onActionTriggered += HandleInput;
        Movement.CanMove = true;
    }

    public void Knockout(Health health)
    {
        onKnockout?.Invoke(this);
        Attack.DisableAttack();
    }

    public void HandleInput(InputAction.CallbackContext ctx)
    {
        if (_isInCutscene) return;

        switch (ctx.action.name)
        {
            case "Move" when ctx.performed:
                Movement.Move(ctx.ReadValue<Vector2>());
                break;
            case "Move" when ctx.canceled:
                Movement.MoveStop();
                break;
            case "Dash" when ctx.performed:
                Movement.Dash();
                break;
            case "Pick" when ctx.performed:
                Hand.Pick();
                break;
            case "Use" when ctx.performed:
                Hand.StartUse();
                break;
            case "Use" when ctx.canceled:
                Hand.StopUse();
                break;
            case "Attack" when ctx.started:
                Attack.OnAttackPressed();
                break;
            case "Attack" when ctx.canceled:
                Attack.OnAttackRelease();
                break;
        }
    }

    public void OnRoundStart()
    {
        Movement.OnRoundStart(RoundStartPosition);
        Hand.OnRoundStart();
        Health.ResetLife();
        _isInCutscene = true;
        Attack.EnableAttack();
        Attack.EnableDamage();
    }

    public void OnFightStart()
    {
        Movement.OnFightStart();
        Hand.OnFightStart();
        _isInCutscene = false;

        Attack.EnableAttack();
        Attack.EnableDamage();
    }

    public void OnFightEnd(CharacterController winner, RoundState.RoundEndType endType)
    {
        Movement.OnFightEnd();
        Hand.OnFightEnd();
        _isInCutscene = true;
        
        if (this == winner)
        {
            RoundsWon++;
            HUD.AddCrown(); // Maybe this could be decoupled by sending an "OnWin" event from CharacterController
        }
        
        Attack.DisableAttack();
    }

    public void OnBucketHit(HeadBucket bucket)
    {
        bucket.transform.SetParent(BucketPosition, false);
        bucket.transform.localPosition = Vector3.zero;
        bucket.transform.localRotation = Quaternion.identity;

        HasBucketOnHead = true;

        Movement.OnBucketHit();
        Hand.OnBucketHit();

        Attack.DisableAttack();
    }

    public void OnBucketEscape()
    {
        HasBucketOnHead = false;

        Movement.OnBucketEscape();
        Hand.OnBucketEscape();
        
        Attack.EnableAttack();
    }

    private void OnMove(float speed)
    {
        Animator.SetFloat("Speed", speed);
    }

    private void OnStartDash()
    {
        Animator.SetTrigger("Dash");
        //Activate Dash VFX
        FXManager.StartDash();
    }

    private void OnEndDash()
    {
        //Desactivate Dash VFX
        FXManager.StopDash();
    }

    private void OnStartCharging(float duration)
    {
        Movement.StartCharging();
        FXManager.StartCharging(duration);
        Animator.Play("Charging");
    }

    private void OnStopCharging()
    {
        Movement.StopCharging();
        FXManager.StopCharging();
        Animator.Play("Charged");
    }
    
    private void OnStartAttack()
    {
        Movement.StartAttaking();
        Animator.SetTrigger("Attack");
    }

    private void OnKick()
    {
        //Movement.CanMove = false;
        Movement.StartAttaking();
        Animator.Play("Kick");
    }

    private void OnPunch()
    {
        //Movement.CanMove = false;
        Movement.StartAttaking();
        Animator.Play("Punch");
    }

    private void OnEndAttack()
    {
        Movement.EndAttaking();
        //Movement.CanMove = true;
    }
}
