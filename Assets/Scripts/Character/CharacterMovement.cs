﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour
{
    public event Action onDashStart;
    public event Action onDashEnd;
    public event Action<float> onStartMove;
    public event Action<float> onStopMove;
    public event Action<float> onStunStart; // float is stun duration
    public event Action onStunEnd;
    public event Action<Vector3> onTurn; 

    private bool _canMove;
    [HideInInspector] public bool CanMove {
        get { return _canMove; }
        set {
            if (!value)
                MoveStop();
            _canMove = value;
        }
    } 

    public float Speed = 5.0f;

    [Header("Speed multipliers")] public float AimingSpeedMult = 0.0f;
    public float AttackingSpeedMult = 0.0f;
    public float ChargingSpeedMult = 0.8f;
    public float HeadBucketSpeedMult = 0.8f;

    [Header("Dash")] public float DashSpeed = 15.0f;
    public float DashMaxDistance = 2.0f;
    public float DashCooldown = 0.5f;
    public int MaxDashStack = 2;
    public float DashRegenTime = 4.0f;

    private bool _isDashing;
    private float _dashMaxTime;
    private float _dashTime = float.MaxValue;
    private Vector3 _dashDirection;

    private int _dashStacks;
    private List<float> _dashResetTimers;

    private Rigidbody _rigidbody;

    private bool _isCharging; // Charge an attack
    private bool _isUsingItem; // Using item slows player down/stops
    private bool _isAttacking; // Attacking slows player down
    private bool _isHeadBucket; // Directions are inverted

    public bool Stunned => _stunTimeLeft > 0.0f;
    private float _stunTimeLeft = float.MinValue;

    private Vector2 _joystick;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _dashMaxTime = DashMaxDistance / DashSpeed;
        _dashResetTimers = new List<float>();
        _dashStacks = MaxDashStack;
    }

    public void OnRoundStart(Transform roundStartPoint)
    {
        CanMove = false;

        _isDashing = false;
        _dashTime = float.MaxValue;

        _isUsingItem = false;
        _isAttacking = false;
        _isHeadBucket = false;
        _dashStacks = MaxDashStack;

        _rigidbody.velocity = Vector3.zero;

        transform.position = roundStartPoint.position;
        LookDirection(roundStartPoint.forward);
    }

    public void OnFightStart()
    {
        CanMove = true;
    }

    public void OnFightEnd()
    {
        Debug.Log("Cant move !");
        CanMove = false;
        _rigidbody.velocity = Vector3.zero;
        LookDirection(Vector3.back);
    }

    private void Update()
    {
        _dashTime += Time.deltaTime;
        
        if (Stunned) {
            _stunTimeLeft -= Time.deltaTime;
            if(!Stunned)
			{
                onStunEnd?.Invoke();
			}
        }

        #region [Dash Timers]

        for (int i = _dashResetTimers.Count - 1; i > -1; i--)
        {
            _dashResetTimers[i] -= Time.deltaTime;
            if (_dashResetTimers[i] <= 0.0f)
            {
                _dashStacks++;
                _dashResetTimers.RemoveAt(i);
            }
        }

        #endregion

        if (!CanMove) return;

        Vector3 dashVelocity = Vector3.zero;

        if (_dashTime < _dashMaxTime)
        {
            dashVelocity = _dashDirection * DashSpeed;
        }
        else if (_isDashing)
        {
            _isDashing = false;
            onDashEnd?.Invoke();
        }

        Vector3 direction = new Vector3(_joystick.x, 0.0f, _joystick.y);
        if (_isHeadBucket)
        {
            direction *= -1;
        }

        float currentSpeed = ComputeCurrentSpeed();
        Vector3 movement = direction * currentSpeed + dashVelocity;

        _rigidbody.velocity = movement;
        LookDirection(direction);
    }

    private float ComputeCurrentSpeed()
    {
        if (Stunned) return 0.0f;

        float currentSpeed = Speed;
        if (_isUsingItem) currentSpeed *= AimingSpeedMult;
        if (_isAttacking && !_isCharging) currentSpeed *= AttackingSpeedMult;
        if (_isCharging) currentSpeed *= ChargingSpeedMult;
        if (_isHeadBucket) currentSpeed *= HeadBucketSpeedMult;
        return currentSpeed;
    }

    public void LookDirection(Vector3 direction)
    {
        transform.LookAt(transform.position + direction);
    }

    public void Move(Vector2 joystick)
    {
        if (!CanMove) return;
     
        onStartMove?.Invoke(joystick.magnitude);
        onTurn?.Invoke(transform.forward);
        _joystick = joystick;
    }

    public void MoveStop()
    {
        if (!CanMove) return;
        
        onStopMove?.Invoke(0);
        _joystick = Vector2.zero;
    }

    public void Dash()
    {
        if (!CanMove || Stunned) return;

        if (_dashStacks == 0) return;
        if (_dashTime < DashCooldown) return;

        _isDashing = true;
        _dashTime = 0.0f;
        _dashDirection = transform.forward;
        
        _dashStacks--;
        _dashResetTimers.Add(DashRegenTime);
        
        onDashStart?.Invoke();
    }

    public void Stun(float duration)
	{
        _stunTimeLeft = duration;
        onStunStart?.Invoke(duration);
        MoveStop();
	}

    public void OnStartUsing(ItemController item)
    {
        _isUsingItem = true;
    }

    public void OnStopUsing(ItemController item, bool success)
    {
        _isUsingItem = false;
    }

    public void OnItemChange(ItemController item)
    {
        _isUsingItem = false;
    }

    public void OnBucketHit()
    {
        _isHeadBucket = true;
    }

    public void OnBucketEscape()
    {
        _isHeadBucket = false;
    }

    internal void StartCharging()
    {
        _isCharging = true;
    }

    internal void StopCharging()
    {
        _isCharging = false;
    }

    internal void StartAttaking()
    {
        _isAttacking = true;
    }

    internal void EndAttaking()
    {
        _isAttacking = false;
    }
}