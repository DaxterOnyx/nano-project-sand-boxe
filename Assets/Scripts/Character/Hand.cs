﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public event Action<ItemController> onStartUse;
    public event Action<ItemController, bool> onStopUse; // Bool is for success
    public event Action<ItemController> onItemChange;

    public Transform StoragePosition;
    public Transform HandPosition;

    private List<ItemController> _accesibleItems;
    private ItemController _currentHeld;

    private bool _canUseItems; // Disabled when the character has a bucket on his head

    public void OnRoundStart()
    {
        _accesibleItems = new List<ItemController>();
        _canUseItems = false;
    }

    public void OnFightStart()
    {
        _canUseItems = true;
    }

    public void OnFightEnd()
	{
        _canUseItems = false;
        _currentHeld = null;
    }

    public void Pick()
	{
        if (!_canUseItems || (_currentHeld && _currentHeld.Using)) return; // Can't pick an item while using another one

        ItemController newItem = PickClosestAccessibleItem();
        if (!newItem) return; // Can't steal items

        DetachCurrentItem(newItem);

        _currentHeld = newItem;
        _currentHeld.onItemBreak += OnItemBreak;
        _currentHeld.Pick(this);

        _currentHeld.Handles.Attach(StoragePosition);

        onItemChange?.Invoke(_currentHeld);
	}

    public void StartUse()
	{
        if (!_canUseItems || !_currentHeld) return;

        if (!_currentHeld.StartUse()) return;
        _currentHeld.Handles.Attach(HandPosition);
        onStartUse?.Invoke(_currentHeld);
	}

    public void StopUse()
	{
        if (!_currentHeld) return;

        _currentHeld.Handles.Attach(StoragePosition);
        bool success = _currentHeld.StopUse();
        onStopUse?.Invoke(_currentHeld, success);
    }

    /**
     * Detaches the current item and drop it at the position of another item
     */
    private void DetachCurrentItem(ItemController newItem)
	{
        if (!_currentHeld) return;

        _currentHeld.Handles.Attach(null);
        _currentHeld.Drop(newItem);
        _currentHeld.onItemBreak -= OnItemBreak;
	}

    /**
     * Handle item break in the hand
     */
    private void OnItemBreak(ItemController item)
	{
        _currentHeld.onItemBreak -= OnItemBreak;
        _currentHeld = null;
        onItemChange?.Invoke(null);
	}

    /**
     * Picks the closest free accessible item near the character
     * Returns null if no accessible item
     */
    private ItemController PickClosestAccessibleItem()
	{
        if (_accesibleItems.Count == 0) return null;

        ItemController item = null;
        float distance = float.MaxValue;
        foreach (ItemController i in _accesibleItems)
		{
            if (i.Owner) continue;
            float d = (transform.position - i.transform.position).sqrMagnitude;
            if (d < distance)
			{
                distance = d;
                item = i;
			}
		}

        return item;
	}

    public void OnBucketHit()
    {
        _canUseItems = false;
    }

    public void OnBucketEscape()
    {
        _canUseItems = true;
    }

	private void OnTriggerEnter(Collider other)
	{
        ItemController item = other.GetComponent<ItemController>();

        if (!item) return;
        if (_accesibleItems.Contains(item)) return;

        _accesibleItems.Add(item);
	}

    private void OnTriggerExit(Collider other)
    {
        ItemController item = other.GetComponent<ItemController>();

        if (!item) return;
        
        _accesibleItems.Remove(item);
    }
}
