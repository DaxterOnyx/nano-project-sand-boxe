﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.VFX;

public class FXManager : MonoBehaviour
{
    //CHARGING VFX
    [FormerlySerializedAs("ColorEffect")] [FoldoutGroup("ChargingVFX"), SerializeField]
    private Color ChargingColorEffect = Color.white;

    [FoldoutGroup("ChargingVFX"), SerializeField]
    private ParticleSystem ChargingEffect;

    [FoldoutGroup("ChargingVFX"), SerializeField]
    private List<SkinnedMeshRenderer> ChargingRenderer;

    [FormerlySerializedAs("MinValue")] [FoldoutGroup("ChargingVFX"), SerializeField]
    private float ChargingMinValue;

    [FormerlySerializedAs("MaxValue")] [FoldoutGroup("ChargingVFX"), SerializeField]
    private float ChargingMaxValue;

    [FormerlySerializedAs("IntensityProgress")] [FoldoutGroup("ChargingVFX"), SerializeField]
    private AnimationCurve ChargingIntensityProgress = AnimationCurve.Linear(0, 0, 1, 1);

    internal float chargingDurationEffect;
    internal float chargingTimeLeft;

    //TRAIL DASH
    [FoldoutGroup("Trail Dash"), SerializeField]
    private TrailRenderer DashTrail;

    [SerializeField]
    private VisualEffect StunVFX;

    [SerializeField]
    private GameObject StepVFXPrefab;

    [SerializeField]
    private Transform LeftFoot;
    [SerializeField]
    private Transform RightFoot;
    private Quaternion _moveDirection;

    private void Awake()
    {
        //CHARGING
        ChargingEffect.Stop();
        SetEffectIntensity(0);
        
        //Dash Trail
        DashTrail.emitting = false;
    }

    private void Update()
    {
        //CHARGING
        if (chargingTimeLeft > 0)
        {
            chargingTimeLeft -= Time.deltaTime;
            SetEffectIntensity((chargingDurationEffect - chargingTimeLeft) / chargingDurationEffect);
        }
    }

    #region Charging Effect

    internal void StartCharging(float timeEffect)
    {
        this.chargingDurationEffect = timeEffect;
        chargingTimeLeft = timeEffect;

        ChargingEffect.Play();
        ChargingEffect.gameObject.SetActive(true);
    }

    internal void StopCharging()
    {
        chargingTimeLeft = -1;
        SetEffectIntensity(0);
        ChargingEffect.Stop();
        ChargingEffect.gameObject.SetActive(false);
    }


    private void SetEffectIntensity(float scale)
    {
        var intensity = ChargingMinValue +
                        ChargingIntensityProgress.Evaluate(scale) * (ChargingMaxValue - ChargingMinValue);
        float r = ChargingColorEffect.r * intensity;
        float g = ChargingColorEffect.g * intensity;
        float b = ChargingColorEffect.b * intensity;
        var colorEffect = new Color(r, g, b, 1);

        foreach (var meshRenderer in ChargingRenderer)
        {
            foreach (var material in meshRenderer.materials)
            {
                material.SetColor("MyColor", colorEffect);
            }
        }
    }

    #endregion

    #region Dash Trail
    
    public void StartDash()
    {
        DashTrail.emitting = true;
    }

    public void StopDash()
    {
        DashTrail.emitting = false;
    }
    #endregion

    #region [Stun]

    public void StartStun(float time)
	{
        StunVFX.SetFloat("StunTime", time);
        StunVFX.Play();
	}

    #endregion

    #region [Footsteps]

    public void OnTurn(Vector3 direction)
	{
        _moveDirection = Quaternion.LookRotation(direction, Vector3.up);
    }

    public void OnLeftFootGrounded()
	{
        GameObject footstep = Instantiate(StepVFXPrefab, LeftFoot.position, _moveDirection);
        Destroy(footstep, 2.0f);
    }

    public void OnRightFootGrounded()
    {
        GameObject footstep = Instantiate(StepVFXPrefab, RightFoot.position, _moveDirection);
        Destroy(footstep, 2.0f);
    }

    #endregion
}