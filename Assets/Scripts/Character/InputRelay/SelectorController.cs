﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SelectorController : MonoBehaviour
{
	public event Action<SelectorController, CharacterController> onSelectCharacter;

	public InputRelay Relay;
	public SelectorColorPool Colors;
	public SelectorMovement Movement;

	private CharacterController _currentTarget;

	public GameObject Meshs;
	public MeshRenderer Glow;
	public MeshRenderer Surface;
	public MeshRenderer Inside;

	private void Start()
	{
		int index = Relay.Input.playerIndex;
		if (index > 1) return;

		Glow.material = Colors.Pool[index].Glow;
		Surface.material = Colors.Pool[index].Surface;
		Inside.material = Colors.Pool[index].Inside;
	}

	private void OnEnable()
	{
		LeanTween.scale(Meshs, Vector3.one, 0.5f).setFrom(Vector3.zero).setEaseSpring();
	}

	private void OnTriggerEnter(Collider other)
	{
		CharacterController character = other.GetComponent<CharacterController>();
		if (!character) return;

		_currentTarget = character;
	}

	private void OnTriggerExit(Collider other)
	{
		CharacterController character = other.GetComponent<CharacterController>();
		if (!character) return;

		_currentTarget = null;
	}

	private void OnValidate()
	{
		if (!_currentTarget) return;
		if (_currentTarget.IsBoundToInput) return;

		onSelectCharacter?.Invoke(this, _currentTarget);
	}

	public void HandleInput(InputAction.CallbackContext ctx)
	{
		switch(ctx.action.name)
		{
			case "Move" when ctx.performed:
				Movement.OnMove(ctx.ReadValue<Vector2>());
				break;
			case "Move" when ctx.canceled:
				Movement.OnStop();
				break;
			case "Validate" when ctx.performed:
				OnValidate();
				break;
		}
	}

}
