﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputRelay : MonoBehaviour
{
    public SelectorController Selector;
	public PlayerInput Input;

	private CharacterController _character;

	/**
	 * Allows the Character Selector to appear visually on the scene and move to choose a character
	 */
	public void EnableBody()
	{
		Selector.gameObject.SetActive(true);
		Input.onActionTriggered += Selector.HandleInput;
		Selector.onSelectCharacter += BindCharacter;
	}

	/**
	 * Disables the ability of the Character Selector to move in the scene
	 */
	public void DisableBody()
	{
		Input.onActionTriggered -= Selector.HandleInput;
		Selector.onSelectCharacter -= BindCharacter;
		Selector.gameObject.SetActive(false);
	}

	private void BindCharacter(SelectorController selector, CharacterController character)
	{
		_character = character;
		DisableBody();

		Input.SwitchCurrentActionMap("gameplay");
		character.BindInputRelay(this);
	}
}
