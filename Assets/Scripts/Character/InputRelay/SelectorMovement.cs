﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorMovement : MonoBehaviour
{
	[SerializeField]
	private Rigidbody _rigidbody;
	public float MoveSpeed = 2.0f;
	public float Inertia = 0.95f;

	private Vector3 _currentVelocity;
	private bool _moving = false;

	private void Update()
	{
		if (!_moving) _currentVelocity *= Inertia;
		_rigidbody.velocity = _currentVelocity;
	}

	public void OnMove(Vector2 direction)
	{
		Vector3 move = new Vector3(direction.x, 0.0f, direction.y).normalized * MoveSpeed;
		_currentVelocity = move;
		_moving = true;
	}

	public void OnStop()
	{
		_moving = false;
	}
}
