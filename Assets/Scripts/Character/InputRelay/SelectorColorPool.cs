﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorPool", menuName = "ScriptableObjects/Spirits/ColorPool")]
public class SelectorColorPool : ScriptableObject
{
    [System.Serializable]
    public struct MaterialList
	{
		public Material Glow;
		public Material Surface;
		public Material Inside;
	}

	public MaterialList[] Pool;
}
