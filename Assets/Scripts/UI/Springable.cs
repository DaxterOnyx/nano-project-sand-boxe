﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Springable : MonoBehaviour
{
    public LTDescr Tween;
    private void OnEnable()
    {
        Tween = LeanTween.scale(gameObject, Vector3.one, 0.5f).setFrom(Vector3.zero).setEaseSpring();
    }
}
