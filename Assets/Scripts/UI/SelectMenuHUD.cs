﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SelectMenuHUD : MonoBehaviour
{
	public GameObject SelectImage;
	public Animator Animator;

	public GameObject StartingText;
	public TextMeshProUGUI StartingCountdown;

    public void StartChooseCharacter()
	{
		Animator.Play("ChooseCharacter");
		StartingText.SetActive(false);
		StartingCountdown.gameObject.SetActive(false);
		SelectImage.SetActive(true);

		SelectImage.GetComponent<Springable>().Tween.setOnComplete(() =>
		{
			LeanTween.scale(SelectImage, Vector3.one * 0.9f, 2.0f)
				.setFrom(Vector3.one)
				.setEaseInOutSine()
				.setLoopPingPong();
		});
	}

	public void StartCountdown(int maxSeconds)
	{
		Animator.Play("Empty");
		StartingText.SetActive(true);
		StartingCountdown.gameObject.SetActive(true);
		SelectImage.SetActive(false);
		UpdateCountdown(maxSeconds);
	}

	public void UpdateCountdown(int seconds)
	{
		// We use this trick to replay the "Boing!" animation
		StartingCountdown.gameObject.SetActive(false);
		StartingCountdown.gameObject.SetActive(true);

		StartingCountdown.text = seconds.ToString();
	}
}
