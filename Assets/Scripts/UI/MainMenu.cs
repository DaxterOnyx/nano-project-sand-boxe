﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public TextMeshProUGUI JoinJ1Text, JoinJ2Text;
	public Animator Animator;
	public CinemachineImpulseSource impulse;

	private void Start()
	{
		Animator.Play("LogoAnim");
	}

	private void OnEnable()
	{
		MenuState.onNewPlayer += UpdatePlayerCountDisplay;
	}

	private void OnDisable()
	{
		MenuState.onNewPlayer -= UpdatePlayerCountDisplay;
	}

	public void UpdatePlayerCountDisplay(InputRelay relay, int number)
	{
		if(number == 1)
        {
			JoinJ1Text.text = "Ready !";
		}
		if (number == 2)
		{
			JoinJ2Text.text = "Ready !";

		}
	}

	public void Shock()
	{
		impulse.GenerateImpulse();
	}
}
