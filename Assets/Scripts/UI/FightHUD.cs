﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FightHUD : MonoBehaviour
{
    public TextMeshProUGUI TimeDisplay;

    public void UpdateTime(float time)
	{
		int minutes = (int) (time / 60.0f);
		int seconds = (int)(time - (float)minutes * 60.0f);

		TimeDisplay.text = minutes + ":" + ((seconds < 10) ? "0" : "") + seconds;
	}
}
