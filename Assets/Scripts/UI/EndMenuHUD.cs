﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndMenuHUD : MonoBehaviour
{
    public Animator Animator;
	public CinemachineImpulseSource impulse;

	public void ShowWinScreen(CharacterController winner)
	{
		// TODO : Find a more flexible way to do that
		Animator.Play(winner.gameObject.name + "Wins");
	}

	public void Shock()
	{
		impulse.GenerateImpulse();
	}
}
