﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsHelp : MonoBehaviour
{
    public Animator Animator;

    public void ShowMinigameControls()
	{
        Animator.Play("ShowMinigameControls");    
    }

    public void ShowCharacterControls()
	{
        Animator.Play("ShowCharacterControls");
    }

    public void HideControls()
    {
        Animator.Play("HideCharacterControls");
    }
}
