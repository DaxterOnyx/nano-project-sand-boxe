﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatesHUD : MonoBehaviour
{
	public event Action onFightCountdownEnd;
	public event Action onFightFinishEnd;
	public event Action onSpawnSprits;

	public Animator Animator;
	public CinemachineImpulseSource impulse;

	#region [Fight Countdown]

	private void Start()
	{
		RoundState.onRoundStart += StartCountdown;
		RoundState.onFightEnd += EndFight;
	}

	private void OnDestroy()
	{
		RoundState.onRoundStart -= StartCountdown;
		RoundState.onFightEnd -= EndFight;
	}

	public void StartChooseCharacter()
	{
		Animator.Play("ChooseCharacter");
	}

	public void StartCountdown()
	{
		Animator.Play("FightCountdown");
	}

	public void SpawnSpirits()
	{
		onSpawnSprits?.Invoke();
	}

	public void StartFight()
	{
		onFightCountdownEnd?.Invoke();
	}

	public void EndFight(CharacterController winner, RoundState.RoundEndType endType)
	{
		switch (endType)
		{
			case RoundState.RoundEndType.KO:
				Animator.Play("KO");
				break;
			case RoundState.RoundEndType.TimesUp:
				Animator.Play("TimesUp");
				break;
		}
		impulse.GenerateImpulse();
		
	}

	public void FinishRound()
	{
		onFightFinishEnd?.Invoke();
	}

	#endregion
}
