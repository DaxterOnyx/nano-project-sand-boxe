﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildIndicator : MonoBehaviour
{
    public Sprite TextureCanBuild;
    public Sprite TextureCantBuild;

    public Image IndicatorImage;

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    public void SetCanBuild(bool canBuild)
	{
        IndicatorImage.sprite = canBuild ? TextureCanBuild : TextureCantBuild;
	}
}
