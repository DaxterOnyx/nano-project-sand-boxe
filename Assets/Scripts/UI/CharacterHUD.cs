﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHUD : SerializedMonoBehaviour
{
    public GameObject[] Crowns;

    [Header("Health")]
    public Image HealthBarImage;
    public Image HealthBarLerpImage;
    public float HealthLerpSpeed = 0.01f;
    public float HealthLerpMaxDelay = 0.3f;

    [Header("Item")]
    public GameObject Item;
    public Texture[] ItemDurationStates;
    public RawImage DurationState;
    public GameObject[] ItemsImages;
    public TextMeshProUGUI UsesCount;

    private float _healthLerpDelay;

    private int _crownsCount;
    private int _itemImageIndex;

    void Start()
    {
        _crownsCount = 0;
        _itemImageIndex = 0;
        _healthLerpDelay = 0.0f;
        Item.SetActive(false);
        foreach (GameObject image in ItemsImages)
            image.SetActive(false);

        foreach (GameObject crown in Crowns)
            crown.SetActive(false);
    }

    public void OnItemChange(ItemController item)
	{
        Item.SetActive(item != null);
        if (!item) return;

        ItemsImages[_itemImageIndex].SetActive(false);
        ItemEffect effect = item.Effect;
        if (effect is WatergunEffect) _itemImageIndex = 0;
        else if (effect is BucketEffect) _itemImageIndex = 1;
        else if (effect is ShovelEffect) _itemImageIndex = 2;
        ItemsImages[_itemImageIndex].SetActive(true);

        UpdateItemMaxUses(effect);
	}

    public void OnItemUse(ItemController item, bool success)
	{
        if (!success || !item) return;
        UpdateItemMaxUses(item.Effect);
    }

    private void UpdateItemMaxUses(ItemEffect item)
	{
        UsesCount.text = item.UsesLeft.ToString();

        int textureIndex = 1;
        if (item.UsesLeft == item.MaxUses) textureIndex = 2;
        else if (item.UsesLeft == 1) textureIndex = 0;
        DurationState.texture = ItemDurationStates[textureIndex];
    }

    public void AddCrown()
	{
        if (_crownsCount >= Crowns.Length) return;

        Crowns[_crownsCount].SetActive(true);
        _crownsCount++;
	}

	private void Update()
	{
        _healthLerpDelay -= Time.deltaTime;
		if (_healthLerpDelay < 0.0f)
		{
            HealthBarLerpImage.fillAmount = Mathf.Lerp(HealthBarLerpImage.fillAmount, HealthBarImage.fillAmount, 0.5f);
        }
	}

	public void OnHealthbarUpdate(Health health, float lifeAmount, float lifePercent)
	{
        HealthBarImage.fillAmount = lifePercent;
        _healthLerpDelay = HealthLerpMaxDelay;
    }
}
