﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameState : ScriptableObject
{
    public static Action<GameState> onStart;
    public static Action<GameState> onEnd;

    public void Begin(GameManager gm)
	{
        onStart?.Invoke(this);
        Init(gm);
	}

    public void End(GameManager gm)
	{
        onEnd?.Invoke(this);
        Clean(gm);
	}

    /**
     * Used to initialize the state
     * Ex: 
     *  - enable objects / interfaces elements
     *  - bind events
     */
    public abstract void Init(GameManager gm);

    /**
     * Equivalent to Update() method
     * Unity does not allow naming a method Update() in ScriptableObject even if there is no Update() callback in this class
     */
    public abstract void Process(GameManager gm);

    /**
     * Used to end the state properly
     * Ex: 
     *  - disable objects / interfaces elements
     *  - unbind events
     */
    public abstract void Clean(GameManager gm);
}
