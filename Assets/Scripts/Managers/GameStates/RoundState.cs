﻿using Cinemachine;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "Round", menuName = "ScriptableObjects/GameStates/RoundState")]
public class RoundState : GameState
{
	public enum RoundEndType
	{
		KO, TimesUp
	}

	public static event Action onRoundStart;
	public static event Action onFightStart;
	public static event Action<CharacterController, RoundEndType> onFightEnd;
	public static event Action onRoundEnd;

	[AssetSelector, InlineEditor(InlineEditorObjectFieldModes.Foldout)]
	public List<MapPreset> Presets;

	[Tooltip("Round duration in seconds")]
	public float RoundDuration = 30.0f;

	[Tooltip("The number of rounds required to win the match")]
	public int WonRoundsRequired = 2;

	private float _roundTime;
	private float _lastUpdateTime;

	private bool _fightStarted;
	private bool _fightEnded;

	private GameObject _endCamera;

	private ObstaclesManager _obstacles;
	private ItemsManager _items;
	private FightHUD _fightHUD;

	public override void Init(GameManager gm)
	{
		_fightEnded = false;
		_fightStarted = false;
		_endCamera = gm.ScaleCamera;
		_fightHUD = gm.FightHUD;

		foreach (CharacterController character in gm.Characters)
		{
			character.onKnockout += OnCharacterKnockout;
			character.Opponent = gm.GetOpponent(character);
		}

		#region [Item Dependencies]

		_obstacles = gm.ObstaclesManager;
		ItemController.onItemSpawn += OnItemSpawn;

		_items = gm.ItemsManager;

		#endregion

		if (Presets.Count > 0)
		{
			MapPreset mapPreset = Presets[Random.Range(0, Presets.Count)];

			gm.ObstaclesManager.SpawnPreset(mapPreset);
			gm.ItemsManager.StartSpawner(mapPreset);
		}

		gm.StatesHUD.onFightCountdownEnd += StartFight;
		gm.StatesHUD.onFightFinishEnd += OnRoundEnd;
		
		gm.FightHUD.gameObject.SetActive(true);
		gm.FightHUD.UpdateTime(RoundDuration);

		onRoundStart?.Invoke();
	}

	private void StartFight()
	{
		_roundTime = 0.0f;
		_lastUpdateTime = float.MinValue;
		_fightStarted = true;
		onFightStart?.Invoke();
	}

	public override void Process(GameManager gm)
	{
		if (_fightEnded) return;
		if (!_fightStarted) return;

		_roundTime += Time.deltaTime;
		if (_roundTime - _lastUpdateTime >= 1.0f)
		{
			gm.FightHUD.UpdateTime(RoundDuration - _roundTime);
			_lastUpdateTime = _roundTime;
		}

		if (_roundTime >= RoundDuration)
		{
			float maxHealth = 0.0f;
			CharacterController maxHealthCharacter = null;
			foreach(CharacterController character in gm.Characters)
			{
				if (character.Health.Amount > maxHealth)
				{
					maxHealth = character.Health.Amount;
					maxHealthCharacter = character;
				}
			}

			EndFight(maxHealthCharacter, RoundEndType.TimesUp);
		}
	}

	public override void Clean(GameManager gm)
	{
		gm.ObstaclesManager.Clear();
		gm.ItemsManager.Clear();

		foreach (CharacterController character in gm.Characters)
		{
			character.onKnockout -= OnCharacterKnockout;
		}

		ItemController.onItemSpawn -= OnItemSpawn;
		gm.StatesHUD.onFightCountdownEnd -= StartFight;
		gm.StatesHUD.onFightFinishEnd -= OnRoundEnd;
	}

	public void OnCharacterKnockout(CharacterController character)
	{
		CharacterController winner = character.Opponent;
		EndFight(winner, RoundEndType.KO);
	}

	public void EndFight(CharacterController winner, RoundEndType endType)
	{
		_fightEnded = true;
		_endCamera.SetActive(true);
		onFightEnd?.Invoke(winner, endType);
		_obstacles.Clear();
		_items.Clear();
		_items.StopSpawner();
		_fightHUD.gameObject.SetActive(false);
	}

	private void OnRoundEnd()
	{
		onRoundEnd?.Invoke();
		_endCamera.SetActive(false);
	}

	/**
	 * Manage item dependencies for the effects
	 */
	public void OnItemSpawn(ItemController item)
	{
		ItemEffect effect = item.GetComponent<ItemEffect>();
		if (!effect) return;

		if (effect is ShovelEffect shovel)
		{
			shovel.Obstacles = _obstacles;
		}
	}
}
