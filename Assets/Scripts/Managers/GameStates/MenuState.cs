﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[CreateAssetMenu(fileName = "Menu", menuName = "ScriptableObjects/GameStates/MenuState")]
public class MenuState : GameState
{
	public static event Action<InputRelay, int> onNewPlayer;
	public static event Action onAllPlayersReady;

	private int _playerCount;

	public override void Init(GameManager gm)
	{
		_playerCount = 0;

		gm.MenuUI.gameObject.SetActive(true);

		gm.InputManager.onPlayerJoined += OnPlayerJoined;
		gm.InputManager.EnableJoining();
	}

	public override void Process(GameManager gm)
	{

	}

	public override void Clean(GameManager gm)
	{
		gm.InputManager.DisableJoining();
		gm.InputManager.onPlayerJoined -= OnPlayerJoined;

		gm.MenuUI.gameObject.SetActive(false);
	}

	private void OnPlayerJoined(PlayerInput playerInput)
	{
		InputRelay relay = playerInput.GetComponent<InputRelay>();
		relay.DisableBody();
		_playerCount ++;
		onNewPlayer?.Invoke(relay, _playerCount);

		// TODO : Put a delay before start
		if (_playerCount == GameManager.PlayerCount)
		{
			onAllPlayersReady?.Invoke();
		}
	}
}
