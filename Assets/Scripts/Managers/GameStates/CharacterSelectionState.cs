﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterSelection", menuName = "ScriptableObjects/GameStates/CharacterSelectionState")]
public class CharacterSelectionState : GameState
{
	public static event Action onStartCountdownEnd;

	public int StartCountdownSeconds = 5;

	private int _selectedPlayerCount;

	private SelectMenuHUD _selectHUD;
	private ControlsHelp _controlsHelp;
	private List<InputRelay> _relays;

	private bool _starting;
	private float _startCoutdown;
	private float _lastUpdate;

	public override void Init(GameManager gm)
	{
		_starting = false;
		_selectedPlayerCount = 0;
		_relays = gm.InputRelays;

		_selectHUD = gm.SelectHUD;

		_controlsHelp = gm.ControlsHelp;
		_controlsHelp.ShowMinigameControls();

		_selectHUD.gameObject.SetActive(true);
		_selectHUD.StartChooseCharacter();

		foreach (InputRelay relay in _relays)
		{
			relay.EnableBody();
			relay.Selector.onSelectCharacter += OnSelectCharacter;
		}
	}

	private void OnSelectCharacter(SelectorController selector, CharacterController character)
	{
		
		if (++_selectedPlayerCount >= GameManager.PlayerCount)
		{
			_starting = true;
			_startCoutdown = (float) StartCountdownSeconds;
			_lastUpdate = float.MaxValue;
			_selectHUD.StartCountdown((int) StartCountdownSeconds);
		} 
		else
		{
			_controlsHelp.ShowCharacterControls();
		}
	}

	public override void Process(GameManager gm)
	{
		if (!_starting) return;

		_startCoutdown -= Time.deltaTime;

		if (_lastUpdate - _startCoutdown >= 1.0f)
		{
			_selectHUD.UpdateCountdown((int) _startCoutdown);
			_lastUpdate = _startCoutdown;
		}

		if (_startCoutdown <= 0.0f) { 
			onStartCountdownEnd?.Invoke();
			_starting = false;
		}
	}

	public override void Clean(GameManager gm)
	{
		foreach (InputRelay relay in gm.InputRelays)
		{
			relay.Selector.onSelectCharacter -= OnSelectCharacter;
		}

		_controlsHelp.HideControls();
		gm.SelectHUD.gameObject.SetActive(false);
	}
}
