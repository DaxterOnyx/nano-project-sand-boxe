﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "End", menuName = "ScriptableObjects/GameStates/EndState")]
public class EndState : GameState
{
	public override void Init(GameManager gm)
	{
		foreach (InputRelay relay in gm.InputRelays)
		{
			relay.Input.onActionTriggered += HandleInput;
			relay.Input.SwitchCurrentActionMap("end");
		}

		gm.EndMenuHUD.ShowWinScreen(gm.GetWinner());
	}

	public override void Process(GameManager gm)
	{

	}

	public override void Clean(GameManager gm)
	{
		foreach (InputRelay relay in gm.InputRelays)
		{
			relay.Input.onActionTriggered -= HandleInput;
		}
	}

	private void OnRestart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	private void HandleInput(InputAction.CallbackContext ctx)
	{
		switch (ctx.action.name)
		{
			case "Restart" when ctx.performed:
				OnRestart();
				break;
		}
	}
}