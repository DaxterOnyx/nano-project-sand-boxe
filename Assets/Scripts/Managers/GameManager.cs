﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    /**
     * This class shouldn't be a Singleton and no other script should directly access to it (except for the GameState derived classes maybe).
     * This class will manage the game state by reacting to the events triggered by the objects in the scene
     * 
     * Ex : The character object emits an event when he gets linked to a player and the CharacterSelectionState
     * handles theses events to start the game & change the game state when he receives two of them
     * 
     * --
     * 
     * This class could be used for the global event bus using static events tho
     */

    public static int PlayerCount = 2;

    [Header("Game states")]
    public MenuState Menu;
    public CharacterSelectionState CharacterSelection;
    public RoundState Round;
    public EndState End;

    // Used to unbind the events later
    Action ChangeStateCharacterSelection;
    Action ChangeStateRound;
    Action ChangeStateRoundOrEnd;

    private GameState _currentGameState;

    [Header("References")]
    public PlayerInputManager InputManager;
    public ObstaclesManager ObstaclesManager;
    public ItemsManager ItemsManager;
    //public CinemachineBrain Camera;
    public GameObject ScaleCamera;
    [HideInInspector] public List<InputRelay> InputRelays;

    public List<CharacterController> Characters;

    [Header("HUD")]
    public MainMenu MenuUI;
    public StatesHUD StatesHUD;
    public FightHUD FightHUD;
    public ControlsHelp ControlsHelp;
    public SelectMenuHUD SelectHUD;
    public EndMenuHUD EndMenuHUD;

    private void Start()
    {
        // Initialize references states
        MenuUI.gameObject.SetActive(false);
        FightHUD.gameObject.SetActive(false);
        SelectHUD.gameObject.SetActive(false);

        ChangeState(Menu);

        MenuState.onNewPlayer += AddInputRelay;
        
        // State machine transitions
        MenuState.onAllPlayersReady += ChangeStateCharacterSelection = () => ChangeState(CharacterSelection);
        CharacterSelectionState.onStartCountdownEnd += ChangeStateRound = () => ChangeState(Round);
        RoundState.onRoundEnd += ChangeStateRoundOrEnd = () =>
        {
            if (GetWinner()) ChangeState(End);
            else ChangeState(Round);
        };
    }

	private void OnDestroy()
	{
        MenuState.onNewPlayer -= AddInputRelay;

        MenuState.onAllPlayersReady -= ChangeStateCharacterSelection;
        CharacterSelectionState.onStartCountdownEnd -= ChangeStateRound;
        RoundState.onRoundEnd -= ChangeStateRoundOrEnd;
    }

	private void Update()
    {
        _currentGameState?.Process(this);
    }

    public void ChangeState(GameState gs)
	{
        _currentGameState?.End(this);
        _currentGameState = gs;
        _currentGameState?.Begin(this);
	}

    public void AddInputRelay(InputRelay relay, int playerCount)
	{
        if (!InputRelays.Contains(relay))
            InputRelays.Add(relay);
	}

    /**
     * Returns the opponent of the character passed in parameter
     */
    public CharacterController GetOpponent(CharacterController character)
	{
        int opponentIndex = character == Characters[0] ? 1 : 0;
        return Characters[opponentIndex];
	}

    /**
     * Returns the winner among the two characters or null if no winner already
     */
    public CharacterController GetWinner()
	{
        foreach (CharacterController character in Characters)
		{
            if (character.RoundsWon >= Round.WonRoundsRequired) 
                return character;
		}
        return null;
	}
}
