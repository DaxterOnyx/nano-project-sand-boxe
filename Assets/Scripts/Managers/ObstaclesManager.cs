﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObstaclesManager : MonoBehaviour
{

	public float PresetSpawnDelay = 0.8f;
	public float ClearDelay = 1.0f;

	public Grid Grid;
	public GameObject SandCastlePrefab;
	public GameObject TirePrefab;

    private Dictionary<Vector2Int, GameObject> _obstacles;

	public void Start()
	{
		_obstacles = new Dictionary<Vector2Int, GameObject>();
		Clear();
	}

	/**
	 * Spawns all the obstacles in a map preset
	 */
	public void SpawnPreset(MapPreset preset)
	{
		List<Vector2Int> Tires = preset.GetAllTilesOfType(MapPreset.TileType.Tire);
		List<Vector2Int> SandCastles = preset.GetAllTilesOfType(MapPreset.TileType.Sandcastle);

		SandCastle castle;

		foreach (Vector2Int position in Tires) StartCoroutine(SpawnTireDelay(position, Random.value * PresetSpawnDelay));
		foreach (Vector2Int position in SandCastles) StartCoroutine(SpawnSandCastleDelay(position, Random.value * PresetSpawnDelay));
	}

	/**
	 * Instanciates a new sand castle on the grid
	 * Returns false if the castle can't be built at the position, true otherwise
	 */
	public bool SpawnSandCastle(Vector2Int position, out SandCastle spawned)
	{
		spawned = null;
		if (!IsConstructible(position)) return false;

		Vector3 worldPosition = Grid.GetPosition(position);
		GameObject sandCastle = Instantiate(SandCastlePrefab, worldPosition, Quaternion.identity);
		_obstacles.Add(position, sandCastle);

		Obstacle o = sandCastle.GetComponent<Obstacle>();
		o.onBreak += Clear;

		spawned = sandCastle.GetComponent<SandCastle>();
		return true;
	}

	public void SpawnTire(Vector2Int position)
	{
		if (!Grid.IsTileValid(position) || _obstacles.ContainsKey(position)) return;

		Vector3 worldPosition = Grid.GetPosition(position);
		GameObject tire = Instantiate(TirePrefab, worldPosition, Quaternion.identity);
		_obstacles.Add(position, tire);

		Obstacle o = tire.GetComponent<Obstacle>();
		o.onBreak += Clear;
	}

	private IEnumerator SpawnSandCastleDelay(Vector2Int position, float delay)
	{
		yield return new WaitForSeconds(delay);
		SandCastle castle;
		SpawnSandCastle(position, out castle);
	}

	private IEnumerator SpawnTireDelay(Vector2Int position, float delay)
	{
		yield return new WaitForSeconds(delay);
		SpawnTire(position);
	}

	/**
	 * Returns true if the tile is empty and a castle can be built here, false otherwise
	 */
	public bool IsConstructible(Vector2Int position)
	{
		return Grid.IsTileValid(position) && !_obstacles.ContainsKey(position);
	}

	public void Clear(Obstacle obstacle)
	{
		foreach (Vector2Int key in _obstacles.Keys)
		{
			if (_obstacles[key] == obstacle.gameObject)
			{
				_obstacles.Remove(key);
				return;
			}
		}
	}

	/**
	 * Clears all the obstacles of the map
	 * TODO : Could be animated with delay
	 */
	public void Clear()
	{
		foreach (GameObject obstacle in _obstacles.Values)
		{
			Obstacle o = obstacle.GetComponent<Obstacle>();
			if (!o) Destroy(obstacle);
			else o.Invoke("Break", Random.value * ClearDelay);
		}
		_obstacles.Clear();
	}
}
