﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class ItemsManager : MonoBehaviour
{
    public Grid Grid;

	public PlayableDirector ProfilesTimeline;
	private SpawnProfile _currentSpawnProfile;

	private List<Vector2Int> _spawnPositions;
	private List<GameObject> _items; // Tracks all the items in the scene
	private Dictionary<Vector2Int, GameObject> _groundItems; // Only tracks items currently on the ground

	private float _timeBeforeNextItem;

	#region [Spawner Profiles]

	/**
	 * Starts the spawner
	 */
	public void StartSpawner(MapPreset preset)
	{
		_spawnPositions = preset.GetAllTilesOfType(MapPreset.TileType.Spawn);
		_currentSpawnProfile = null;
		ProfilesTimeline.Play();
	}

    public void StopSpawner()
	{
		_currentSpawnProfile = null;
        ProfilesTimeline.Stop();
	}

    public void ChangeSpawnProfile(SpawnProfile newProfile)
	{
        if (!newProfile) return;
		SpawnProfile oldProfile = _currentSpawnProfile;
        _currentSpawnProfile = newProfile;
		
		if (!oldProfile) // If this is the first spawner profile
		{
			InitSpawner();
		}
	}

	#endregion

	#region [Item Spawning]

	/**
	 * Initializes the spawning process
	 */
	private void InitSpawner()
	{
		_groundItems = new Dictionary<Vector2Int, GameObject>();
		_items = new List<GameObject>();
		_timeBeforeNextItem += _currentSpawnProfile.GetRandomDelay();
	}

	/**
	 * Spawns a random item from the current pool on an empty spawn spot
	 */
	private void SpawnItem()
	{
		if (_items.Count >= _currentSpawnProfile.MaxItems) return;

		GameObject prefab = _currentSpawnProfile.GetRandomItem();
		Vector2Int? spawnTile = GetRandomFreeSpawnPosition();
		if (spawnTile == null) return; // If there is no free spawn position

		GameObject newItem = Instantiate(prefab);
		PlaceItemOnGround(newItem, spawnTile.Value);

		_items.Add(newItem);

		ItemController ic = newItem.GetComponent<ItemController>();

		ic.onPick += OnItemPick;
		ic.onDrop += OnItemDrop;
		ic.onItemBreak += OnItemBreak;
	}

	private void PlaceItemOnGround(GameObject item, Vector2Int tile)
	{
		float randomRotation = Random.Range(0.0f, 360.0f);

		Vector3 spawnPosition = Grid.GetPosition(tile);
		item.transform.position = spawnPosition;
		item.transform.rotation = Quaternion.AngleAxis(randomRotation, Vector3.up);
		_groundItems.Add(tile, item);
	}

	/**
	 * Removes the item from the ground tile
	 * Returns the new free file
	 * The item isn't destroyed from the scene
	 */
	private Vector2Int? UngroundItem(GameObject item)
	{
		foreach (Vector2Int key in _groundItems.Keys)
		{
			if (_groundItems[key] == item) {
				_groundItems.Remove(key);
				return key;
			}
		}
		return null;
	}

	private void OnItemPick(ItemController item, Hand owner)
	{
		UngroundItem(item.gameObject);
	}

	private void OnItemDrop(ItemController item, ItemController replacement)
	{
		if (replacement.Owner) return; // The replacement isn't on the ground
		if (!_items.Contains(replacement.gameObject)) return; // Not a valid replacement
		
		// TODO : Find a cleaner way to swap items
		Vector2Int? tile = UngroundItem(replacement.gameObject);
		PlaceItemOnGround(item.gameObject, tile.Value);
	}

	private void OnItemBreak(ItemController item)
	{
		UngroundItem(item.gameObject);
		_items.Remove(item.gameObject);
	}

	private void Update()
	{
		if (!_currentSpawnProfile) return; // Ensure there is an active spawn profile to run the spawning process

		_timeBeforeNextItem -= Time.deltaTime;
		if (_timeBeforeNextItem <= 0.0f)
		{
			SpawnItem();
			_timeBeforeNextItem = _currentSpawnProfile.GetRandomDelay();
		}
	}

	/**
	 * Returns a random spawning position depending on the current map preset
	 * Returns null if there is no free
	 * 
	 * Finds an free spawn position in O(n)
	 */
	private Vector2Int? GetRandomFreeSpawnPosition()
	{
		if (_spawnPositions.Count - _groundItems.Count <= 0) return null;
		int iterations = Random.Range(0, _spawnPositions.Count - _groundItems.Count);
		int index = -1;
		do
		{
			index++;
			if (!_groundItems.ContainsKey(_spawnPositions[index])) iterations--;
		} while (iterations >= 0);

		return _spawnPositions[index];
	}

	#endregion

	/**
	 * Clears all the items of the map even those help by players
	 */
	public void Clear()
	{
		foreach (GameObject item in _items)
		{
			Destroy(item);
		}
		_items.Clear();
		_groundItems.Clear();
	}
}
