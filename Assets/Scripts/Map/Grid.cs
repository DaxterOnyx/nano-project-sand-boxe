﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This simple class manages the grid system of the map
 * 
 * /!\ It does not instantiate any object nor checking if a position is free for an object to spawn there.
 * This task is left to the respective managers of each items
 */
public class Grid : SerializedMonoBehaviour
{
    public Vector2Int Size;
    public float TileSize = 1.0f;

    [Header("Debug")]
    public float GizmosHeight = 0.1f;

    private Vector3 _offset;

	private void Start()
	{
        _offset = new Vector3(TileSize / 2.0f, 0.0f, TileSize / 2.0f);
    }

	/**
     * Returns true if the tile is inside the grid, false otherwise
     */
	public bool IsTileValid(Vector2Int tile)
	{
        return 0 <= tile.x && tile.x < Size.x && 0 <= tile.y && tile.y < Size.y;
    }

    /**
     * Returns the tile position that corresponds to the given position
     */
    public Vector2Int GetTile(Vector3 position)
	{
        Vector3 relativePosition = position - transform.position;

        int x = Mathf.FloorToInt((relativePosition.x) / TileSize);
        int y = Mathf.FloorToInt((relativePosition.z) / TileSize);
        return new Vector2Int(x, y);
    }

    /**
     * Returns the 3D position of a tile on the grid
     */
    public Vector3 GetPosition(Vector2Int tile)
    {
        return transform.position + new Vector3(tile.x, 0.0f, tile.y) + _offset;
    }

    /**
     * Returns the given position aligned to the closest grid tile
     */
    public Vector3 Align(Vector3 position)
	{
        Vector2Int tile = GetTile(position);
        return GetPosition(tile);
	}

	private void OnDrawGizmosSelected()
	{
        Gizmos.color = Color.red;

        for (int y = 0; y <= Size.y; y++)
        {
            Vector3 start = new Vector3(0.0f, GizmosHeight, y * TileSize);
            Vector3 end = new Vector3(Size.x * TileSize, GizmosHeight, y * TileSize);
            Gizmos.DrawLine(transform.position + start, transform.position + end);
        }

        for (int x = 0; x <= Size.x; x++)
        {
            Vector3 start = new Vector3(x * TileSize, GizmosHeight, 0.0f);
            Vector3 end = new Vector3(x * TileSize, GizmosHeight, Size.y * TileSize);
            Gizmos.DrawLine(transform.position + start, transform.position + end);
        }
    }
}
