﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class SandCastle : MonoBehaviour
{
    public Obstacle obstacle;
    public Health health;
    public float StunDuration = 1.5f;
    public float StunRange = 0.6f;
    public VisualEffect SpawnVFX;
    public VisualEffect DespawnFX;
    public GameObject DestroyVFX;

    void Start()
    {
        health.ResetLife();
        health.onDeath += OnDeath;
        SpawnVFX.Play();
    }

    private void OnDeath(Health health)
	{
        obstacle.Break();
        Instantiate(DestroyVFX, transform.position, Quaternion.identity);
	}

    public void CheckCollisionsForStun(CharacterController owner)
	{
        CharacterMovement ownerMovement = owner.GetComponent<CharacterMovement>();

        Collider[] colliders = Physics.OverlapSphere(transform.position, StunRange);
        foreach (Collider col in colliders)
		{
            CharacterMovement character = col.GetComponent<CharacterMovement>();
            if (character && character != ownerMovement) {
                character.Stun(StunDuration);
            }
        }
	}
}
