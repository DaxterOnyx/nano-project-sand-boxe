﻿using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Preset", menuName = "ScriptableObjects/Map/Preset")]
public class MapPreset : SerializedScriptableObject
{
    private static Color[] _editorTileColors = new Color[] { Color.gray, Color.yellow, Color.red, Color.black};
    public enum TileType
	{
        Nothing, Sandcastle, Spawn, Tire
	}

    [InfoBox("Yellow : sand castle\nRed : spawning zone\nBlack : tire\nRight-Click : Clear")]
    [TableMatrix(HorizontalTitle = "Castles", SquareCells = true, DrawElementMethod = "DrawTileType")]
    public TileType[,] Tiles = new TileType[15, 11];

    /**
     * Returns a list of all item spawn positions depending on the CurrentPreset
     * (note: left in a dedicated function despite the generic one in case the spawn spots map is split from the obstacles)
     */
    public List<Vector2Int> GetItemSpawnPositions()
    {
        List<Vector2Int> spawnPositions = new List<Vector2Int>();
        for (int y = 0; y < Tiles.GetLength(1); y++)
        {
            for (int x = 0; x < Tiles.GetLength(0); x++)
            {
                if (Tiles[x, y] == MapPreset.TileType.Spawn)
                {
                    spawnPositions.Add(new Vector2Int(x, y));
                }
            }
        }
        return spawnPositions;
    }

    /**
     * Returns a list of all tiles position of a certain type
     */
    public List<Vector2Int> GetAllTilesOfType(TileType type)
    {
        List<Vector2Int> spawnPositions = new List<Vector2Int>();
        for (int y = 0; y < Tiles.GetLength(1); y++)
        {
            for (int x = 0; x < Tiles.GetLength(0); x++)
            {
                if (Tiles[x, y] == type)
                {
                    spawnPositions.Add(new Vector2Int(x, y));
                }
            }
        }
        return spawnPositions;
    }

    private static TileType DrawTileType(Rect rect, TileType value)
	{
        if (Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
		{
            if (Event.current.button == 0)
                value = (TileType)(((int)value + 1) % Enum.GetNames(typeof(TileType)).Length);
            else if (Event.current.button == 1)
                value = TileType.Nothing;

            GUI.changed = true;
            Event.current.Use();
        }
#if UNITY_EDITOR
        EditorGUI.DrawRect(rect.Padding(1), _editorTileColors[(int)value]);
#endif
        return value;
	}
}
