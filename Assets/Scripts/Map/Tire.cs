﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tire : MonoBehaviour
{
    public GameObject[] Variants;
    public Transform MeshContainer;

    void Start()
    {
        int index = Random.Range(0, Variants.Length);
        GameObject variant = Instantiate(Variants[index]);
        variant.transform.SetParent(MeshContainer);
        variant.transform.localPosition = Vector3.zero;
    }
}
