﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public event Action onStartDestroy;
    public event Action<Obstacle> onBreak;

    public GameObject Mesh;
    public float MeshHeight;
    
    void Start()
    {
        LeanTween.moveLocalY(Mesh, 0,0.4f).setEaseSpring().setFrom(-MeshHeight);
    }

	private void OnDestroy()
	{
        onBreak?.Invoke(this);
	}

	public void Break()
	{
        // We add a safe offset
        onStartDestroy?.Invoke();
        LeanTween.moveLocalY(Mesh, - (MeshHeight + 0.1f), 0.4f).setEaseOutCubic().setOnComplete(() =>
        {
            Destroy(gameObject);
        });
    }
}
