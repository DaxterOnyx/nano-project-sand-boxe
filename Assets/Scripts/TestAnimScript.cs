﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestAnimScript : MonoBehaviour
{
    public bool move = false;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<PlayerInput>().onActionTriggered += GetComponent<CharacterController>().HandleInput;
        var characterMovement = GetComponent<CharacterMovement>();
        characterMovement.CanMove = true;
        if (!move)
        {
            characterMovement.Speed = 0;
            characterMovement.DashSpeed = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}